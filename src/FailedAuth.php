<?php

namespace Drupal\authenticate_by_mail;

use Drupal\user\UserAuthInterface;

/**
 * Forces all user credentials to fail authentication.
 *
 * This conflicts with the 'basic_auth' module, or any other module which
 * depends on password-based user authentication.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class FailedAuth implements UserAuthInterface {

  /**
   * {@inheritdoc}
   */
  public function authenticate($username, $password) {
    return FALSE;
  }

}
