<?php

namespace Drupal\authenticate_by_mail\Controller;

use Drupal\Component\Datetime\TimeInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Flood\FloodInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Facilitates authentication by one-time login link.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class LoginController extends ControllerBase {

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs an AuthenticationController object.
   *
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(FloodInterface $flood, TimeInterface $time) {
    $this->flood = $flood;
    $this->time = $time;
  }

  /**
   * Attempt to authenticate a user using the specified parameters.
   *
   * This controller must always return a redirect response in order to avoid
   * disclosing a login link in the user agent's referrer headers.
   *
   * @param int $uid
   *   User ID of the user requesting reset.
   * @param int $timestamp
   *   The current timestamp.
   * @param string $hash
   *   Login link hash.
   */
  public function authenticate($uid, $timestamp, $hash) {
    /** @var \Drupal\user\UserInterface|null */
    $target_user = $this->entityTypeManager()->getStorage('user')->load($uid);
    $current_time = $this->time->getRequestTime();

    // Checks that the user agent is not already logged in.
    if ($this->currentUser()->isAuthenticated()) {
      $this->messenger()->addError($this->t('You must log out before you can attempt to log in as another user.'));
      return $this->redirect('<front>');
    }

    // Checks that the target user exists and isn't blocked.
    if (!$target_user || !$target_user->isActive()) {
      $this->messenger()->addError($this->t('The one-time login link you clicked is invalid.'));
      return $this->redirect('<front>');
    }

    // Checks that the link has not expired (not enforced for new users).
    if ($target_user->getLastLoginTime() && $current_time - $timestamp > $this->getLoginLinkTimeout()) {
      $this->messenger()->addError($this->t('You have tried to use a one-time login link that has expired.'));
      return $this->redirect('user.login');
    }

    // Checks that all of the following are true:
    // - The target user is a regular account.
    // - The target user has not logged in after the link was generated.
    // - The link was generated in the past.
    // - The hash matches the expected value.
    if (!$target_user->isAuthenticated() || $timestamp < $target_user->getLastLoginTime() || $timestamp > $current_time || !\hash_equals($hash, \user_pass_rehash($target_user, $timestamp))) {
      $this->messenger()->addError($this->t('You have tried to use a one-time login link that has either been used or is no longer valid.'));
      return $this->redirect('user.login');
    }

    // By this point, the user agent has passed all authentication steps.
    // - Clear the flood control mechanism for the target user.
    // - Finalize the login of the target user.
    $this->flood->clear('authenticate_by_mail.flood_request_user', $target_user->id());
    \user_login_finalize($target_user);

    // Redirect the logged-in user to the front page.
    return $this->redirect('<front>');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('flood'),
      $container->get('datetime.time'),
    );
  }

  /**
   * Get the timeout (in seconds) for one-time login links.
   *
   * @return int
   *   The timeout (in seconds) for one-time login links.
   */
  protected function getLoginLinkTimeout(): int {
    return $this->config('authenticate_by_mail.settings')->get('timeout');
  }

}
