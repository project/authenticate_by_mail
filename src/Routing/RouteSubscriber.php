<?php

namespace Drupal\authenticate_by_mail\Routing;

use Drupal\authenticate_by_mail\Form\LoginForm;

use Drupal\Core\Routing\RouteSubscriberBase;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alters routes relating to user authentication.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * Alter the user login route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The user login route.
   */
  protected function alterLoginRoute(Route $route): void {
    $route->setDefaults([
      '_form' => LoginForm::class,
      '_title' => 'Log in',
    ]);

    $route->setOption('_maintenance_access', TRUE);
    $route->setRequirements([
      '_user_is_logged_in' => 'FALSE',
    ]);
  }

  /**
   * Alter the password reset route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The password reset route.
   */
  protected function alterPasswordResetRoute(Route $route): void {
    $route->setOption('_maintenance_access', FALSE);
    $route->setRequirements([
      '_access' => 'FALSE',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $login_route = $collection->get('user.login');
    $password_reset_route = $collection->get('user.pass');

    if (!$login_route) {
      throw new \RuntimeException('Unable to load route "user.login"');
    }

    if (!$password_reset_route) {
      throw new \RuntimeException('Unable to load route "user.pass"');
    }

    $this->alterLoginRoute($login_route);
    $this->alterPasswordResetRoute($password_reset_route);
  }

}
