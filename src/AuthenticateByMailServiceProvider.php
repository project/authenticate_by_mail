<?php

namespace Drupal\authenticate_by_mail;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

use Symfony\Component\DependencyInjection\Definition;

/**
 * Replaces the user authentication service.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class AuthenticateByMailServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('user.auth')) {
      $service = new Definition(FailedAuth::class);
      $service->setPublic(TRUE);

      $container->setDefinition('user.auth', $service);
    }
  }

}
