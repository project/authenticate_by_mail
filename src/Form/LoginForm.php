<?php

namespace Drupal\authenticate_by_mail\Form;

use Drupal\Component\Utility\EmailValidatorInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Email;
use Drupal\Core\TypedData\TypedDataManagerInterface;

use Drupal\user\UserInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form that users can use to receive a login link by mail.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class LoginForm extends FormBase {

  /**
   * The email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * The typed data manager service.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * Constructs a LoginForm object.
   *
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   The typed data manager service.
   */
  public function __construct(EmailValidatorInterface $email_validator, EntityTypeManagerInterface $entity_type_manager, FloodInterface $flood, TypedDataManagerInterface $typed_data_manager) {
    $this->emailValidator = $email_validator;
    $this->entityTypeManager = $entity_type_manager;
    $this->flood = $flood;
    $this->typedDataManager = $typed_data_manager;
  }

  /**
   * Check if a flood event is allowed, and register one if so.
   *
   * @param string $type
   *   The type of flood event.
   * @param string|null $identifier
   *   Unique identifier of the current user (default: NULL).
   *
   * @return bool
   *   TRUE if allowed, FALSE otherwise.
   */
  protected function checkFloodIsAllowedAndRegister(string $type, ?string $identifier = NULL): bool {
    $config = $this->config('authenticate_by_mail.settings');

    $name = "authenticate_by_mail.{$type}";
    $threshold = $config->get("{$type}.threshold");
    $window = $config->get("{$type}.window");

    if ($this->flood->isAllowed($name, $threshold, $window, $identifier)) {
      $this->flood->register($name, $window, $identifier);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('email.validator'),
      $container->get('entity_type.manager'),
      $container->get('flood'),
      $container->get('typed_data_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#cache']['contexts'][] = 'url.query_args';

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username or email address'),
      '#description' => $this->t('A one-time login link will be sent to your registered email address.'),
      '#maxlength' => \max(UserInterface::USERNAME_MAX_LENGTH, Email::EMAIL_MAX_LENGTH),
      '#required' => TRUE,
      '#size' => 60,
      '#attributes' => [
        'autocorrect' => 'off',
        'autocapitalize' => 'off',
        'spellcheck' => 'false',
        'autofocus' => 'autofocus',
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'authenticate_by_mail_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\user\UserInterface|null */
    if ($user = $form_state->getTemporaryValue('user')) {
      if (_authenticate_by_mail_notify($user)) {
        $this->logger('authenticate_by_mail')->info('One-time login link mailed to %name at %email.', [
          '%name' => $user->getAccountName(),
          '%email' => $user->getEmail(),
        ]);
      }
    }
    elseif (empty($form_state->getTemporaryValue('flood'))) {
      $this->logger('authenticate_by_mail')->info('Login form was submitted with an unknown or inactive account: %name.', [
        '%name' => $form_state->getValue('name'),
      ]);
    }

    $this->messenger()->addStatus($this->t('If %name is a valid account, an email was sent with a one-time login link.', [
      '%name' => $form_state->getValue('name'),
    ]));

    $form_state->setRedirect('<front>');
  }

  /**
   * Validates the supplied string as an email address.
   *
   * @param string $name
   *   The string to validate.
   *
   * @return bool
   *   TRUE if the supplied string is a valid email address, FALSE otherwise.
   */
  protected function validateEmail(string $name): bool {
    return $this->emailValidator->isValid($name);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $name = \trim($form_state->getValue('name', ''));

    if (!$this->checkFloodIsAllowedAndRegister('flood_request_ip')) {
      $form_state->setErrorByName('name', $this->t('Too many login requests from your IP address. It is temporarily blocked. Try again later or contact the site administrator.'));
      return;
    }

    if (!$this->validateUsername($name) && !$this->validateEmail($name)) {
      $form_state->setErrorByName('name', $this->t('The username or email address is invalid.'));
      return;
    }

    $user_storage = $this->entityTypeManager->getStorage('user');
    $user = NULL;

    $user_query = $user_storage->getQuery('OR');
    $user_query->accessCheck(FALSE);
    $user_query->condition('mail', $name);
    $user_query->condition('name', $name);
    $user_query->range(0, 1);

    // Check if a UID was found that corresponds to the user-supplied input.
    if (0 < $uid = \current($user_query->execute())) {
      /** @var \Drupal\user\UserInterface|null */
      $user = $user_storage->load($uid);
    }

    // Blocked accounts cannot request a new password.
    if ($user && $user->id() && $user->isActive()) {
      $is_allowed = $this->checkFloodIsAllowedAndRegister('flood_request_user', $user->id());
      $form_state->setTemporaryValue('flood', !$is_allowed);

      if ($is_allowed) {
        $form_state->setTemporaryValue('user', $user);
      }
    }
  }

  /**
   * Validates the supplied string as a username.
   *
   * @param string $name
   *   The string to validate.
   *
   * @return bool
   *   TRUE if the supplied string is a valid username, FALSE otherwise.
   */
  protected function validateUsername(string $name): bool {
    $definition = BaseFieldDefinition::create('string');
    $definition->addConstraint('UserName', []);

    $data = $this->typedDataManager->create($definition);
    $data->setValue($name);

    return \count($data->validate()) === 0;
  }

}
