<?php

namespace Drupal\authenticate_by_mail\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a settings form for this module.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(\current($this->getEditableConfigNames()));
    $form['#tree'] = TRUE;

    $form['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Login link timeout'),
      '#description' => $this->t('Enter the validity period (in seconds) for each one-time login link. This is not enforced for new users who have never logged in.'),
      '#default_value' => $config->get('timeout'),
      '#required' => TRUE,
      '#max' => \PHP_INT_MAX,
      '#min' => 1,
      '#step' => 1,
    ];

    $form['flood_request_ip'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('IP address flood limit'),
    ];

    $form['flood_request_ip']['threshold'] = [
      '#type' => 'number',
      '#title' => $this->t('Threshold'),
      '#description' => $this->t('Enter the number of requests that can be made per unit of time by IP address.'),
      '#default_value' => $config->get('flood_request_ip.threshold'),
      '#required' => TRUE,
      '#max' => \PHP_INT_MAX,
      '#min' => 1,
      '#step' => 1,
    ];

    $form['flood_request_ip']['window'] = [
      '#type' => 'number',
      '#title' => $this->t('Window'),
      '#description' => $this->t('Enter the amount of time (in seconds) used to track requests by IP address.'),
      '#default_value' => $config->get('flood_request_ip.window'),
      '#required' => TRUE,
      '#max' => \PHP_INT_MAX,
      '#min' => 1,
      '#step' => 1,
    ];

    $form['flood_request_user'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('User flood limit'),
    ];

    $form['flood_request_user']['threshold'] = [
      '#type' => 'number',
      '#title' => $this->t('Threshold'),
      '#description' => $this->t('Enter the number of requests that can be made per unit of time for each user.'),
      '#default_value' => $config->get('flood_request_user.threshold'),
      '#required' => TRUE,
      '#max' => \PHP_INT_MAX,
      '#min' => 1,
      '#step' => 1,
    ];

    $form['flood_request_user']['window'] = [
      '#type' => 'number',
      '#title' => $this->t('Window'),
      '#description' => $this->t('Enter the amount of time (in seconds) used to track requests for each user.'),
      '#default_value' => $config->get('flood_request_user.window'),
      '#required' => TRUE,
      '#max' => \PHP_INT_MAX,
      '#min' => 1,
      '#step' => 1,
    ];

    $form['login'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Login Email'),
      '#description' => $this->t('Edit the email message sent to users who request a one-time login link. Available variables are: [site:name], [site:url], [user:display-name], [user:account-name], [user:mail], [site:login-url], [site:url-brief], [user:one-time-login-url].'),
    ];

    $form['login']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config->get('login.subject'),
      '#maxlength' => 180,
    ];

    $form['login']['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $config->get('login.body'),
      '#rows' => 12,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['authenticate_by_mail.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'authenticate_by_mail_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->addCleanValueKey('actions');

    $config = $this->config(\current($this->getEditableConfigNames()));
    $config->setData($form_state->cleanValues()->getValues());
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
