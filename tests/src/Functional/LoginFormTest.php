<?php

namespace Drupal\Tests\authenticate_by_mail\Functional;

use Drupal\Core\Test\AssertMailTrait;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the login form.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @group authenticate_by_mail
 */
class LoginFormTest extends BrowserTestBase {

  use AssertMailTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'authenticate_by_mail',
  ];

  /**
   * Tests IP address flood control.
   */
  public function testFloodByAddress() {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface */
    $config_factory = $this->container->get('config.factory');
    $assert_session = $this->assertSession();

    $settings = $config_factory->getEditable('authenticate_by_mail.settings');
    $settings->set('flood_request_ip.threshold', 1);
    $settings->save(TRUE);

    $user = $this->drupalCreateUser();
    $mail_filter = [
      'id' => 'authenticate_by_mail_login',
    ];

    $this->drupalGet('/user');
    $this->submitForm([
      'name' => $user->getAccountName(),
    ], 'Submit');

    $assert_session->statusMessageContains("If {$user->getAccountName()} is a valid account, an email was sent with a one-time login link.");
    $this->assertCount(1, $this->getMails($mail_filter), 'One-time login link was mailed');

    $this->drupalGet('/user');
    $this->submitForm([
      'name' => $user->getAccountName(),
    ], 'Submit');

    $assert_session->statusMessageContains('Too many login requests from your IP address. It is temporarily blocked. Try again later or contact the site administrator.');
    $this->assertCount(1, $this->getMails($mail_filter), 'Additional one-time login link was not mailed');
  }

  /**
   * Tests user-specific flood control.
   */
  public function testFloodByUser() {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface */
    $config_factory = $this->container->get('config.factory');
    $assert_session = $this->assertSession();

    $settings = $config_factory->getEditable('authenticate_by_mail.settings');
    $settings->set('flood_request_user.threshold', 1);
    $settings->save(TRUE);

    $user = $this->drupalCreateUser();
    $mail_filter = [
      'id' => 'authenticate_by_mail_login',
    ];

    $this->drupalGet('/user');
    $this->submitForm([
      'name' => $user->getAccountName(),
    ], 'Submit');

    $assert_session->statusMessageContains("If {$user->getAccountName()} is a valid account, an email was sent with a one-time login link.");
    $this->assertCount(1, $this->getMails($mail_filter), 'One-time login link was mailed');

    $this->drupalGet('/user');
    $this->submitForm([
      'name' => $user->getAccountName(),
    ], 'Submit');

    $assert_session->statusMessageContains("If {$user->getAccountName()} is a valid account, an email was sent with a one-time login link.");
    $this->assertCount(1, $this->getMails($mail_filter), 'Additional one-time login link was not mailed');
  }

  /**
   * Tests one-time login links.
   */
  public function testLoginLink() {
    $user = $this->drupalCreateUser();
    $mail_filter = [
      'id' => 'authenticate_by_mail_login',
    ];

    $this->drupalGet('/user');
    $this->submitForm([
      'name' => $user->getAccountName(),
    ], 'Submit');

    $mail = \current($this->getMails($mail_filter));
    $mail = \is_array($mail) && \array_key_exists('body', $mail) ? $mail['body'] : NULL;

    $this->assertIsString($mail);
    $this->assertEquals(1, \preg_match('|\\S+/authenticate-by-mail/\\d+/\\d+/\\S+|', $mail, $matches));
    $this->assertNotEmpty($matches);
    $this->assertNotEmpty($login_url = $matches[0]);

    $user->sessionId = $this->getSession()->getCookie(\Drupal::service('session_configuration')->getOptions(\Drupal::request())['name']);
    $this->assertFalse($this->drupalUserIsLoggedIn($user), 'User is not logged in');

    $this->drupalGet($login_url);

    $user->sessionId = $this->getSession()->getCookie(\Drupal::service('session_configuration')->getOptions(\Drupal::request())['name']);
    $this->assertTrue($this->drupalUserIsLoggedIn($user), 'User is logged in');
  }

}
