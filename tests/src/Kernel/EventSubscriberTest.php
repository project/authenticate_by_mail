<?php

namespace Drupal\Tests\authenticate_by_mail\Kernel;

use Drupal\authenticate_by_mail\FailedAuth;

use Drupal\KernelTests\KernelTestBase;

use Drupal\user\UserAuthInterface;

/**
 * Tests the event subscribers implemented by this module.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @group authenticate_by_mail
 */
class EventSubscriberTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
  ];

  /**
   * Tests that the password reset form is accessible out of the box.
   */
  public function testPasswordResetFormRouteAccess() {
    /** @var \Drupal\Core\Access\AccessManagerInterface */
    $access_manager = $this->container->get('access_manager');

    $access_result = $access_manager->checkNamedRoute('user.pass');
    $this->assertTrue($access_result, 'Password reset form route is accessible before module install');
  }

  /**
   * Tests that the password reset form is inaccessible after module install.
   *
   * @covers \Drupal\authenticate_by_mail\Routing\RouteSubscriber
   */
  public function testPasswordResetFormRouteAccessWasAltered() {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface */
    $module_installer = $this->container->get('module_installer');
    $module_installer->install([
      'authenticate_by_mail',
    ]);

    /** @var \Drupal\Core\Access\AccessManagerInterface */
    $access_manager = $this->container->get('access_manager');

    $access_result = $access_manager->checkNamedRoute('user.pass');
    $this->assertFalse($access_result, 'Password reset form route is inaccessible after module install');
  }

  /**
   * Tests that the user authentication service exists out of the box.
   */
  public function testUserAuthenticationServiceExists() {
    $user_auth = $this->container->get('user.auth');
    $this->assertEquals($user_auth instanceof UserAuthInterface, 'User authentication service exists');
  }

  /**
   * Tests that the user authentication service was replaced.
   *
   * @covers \Drupal\authenticate_by_mail\AuthenticateByMailServiceProvider
   */
  public function testUserAuthenticationServiceWasReplaced() {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface */
    $module_installer = $this->container->get('module_installer');
    $module_installer->install([
      'authenticate_by_mail',
    ]);

    $user_auth_class = \get_class($this->container->get('user.auth'));
    $this->assertEquals(FailedAuth::class, $user_auth_class, 'User authentication service was replaced');
  }

}
