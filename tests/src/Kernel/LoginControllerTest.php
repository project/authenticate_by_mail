<?php

namespace Drupal\Tests\authenticate_by_mail\Kernel;

use Drupal\authenticate_by_mail\Controller\LoginController;

use Drupal\Core\Messenger\MessengerInterface;

use Drupal\KernelTests\KernelTestBase;

use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests the login controller.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @covers \Drupal\authenticate_by_mail\Controller\LoginController
 * @group authenticate_by_mail
 */
class LoginControllerTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'authenticate_by_mail',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installSchema('user', 'users_data');

    $this->installConfig([
      'authenticate_by_mail',
      'system',
    ]);
  }

  /**
   * Tests login link behavior while already logged in.
   */
  public function testAlreadyLoggedIn() {
    $user1 = $this->createUser();
    $user2 = $this->createUser();

    $this->setCurrentUser($user1);

    /** @var \Drupal\authenticate_by_mail\Controller\LoginController */
    $login_controller = \Drupal::classResolver(LoginController::class);
    $login_controller->authenticate($user2->id(), $timestamp = \time(), \user_pass_rehash($user2, $timestamp));

    $this->assertCount(1, $messages = \Drupal::messenger()->messagesByType(MessengerInterface::TYPE_ERROR));
    $this->assertContainsEquals('You must log out before you can attempt to log in as another user.', $messages);

    $this->assertSame($user1, \Drupal::currentUser()->getAccount(), 'Current user did not change');
  }

  /**
   * Tests login link behavior for blocked users.
   */
  public function testBlockedUser() {
    $user = $this->createUser();
    $user->block()->save();

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is anonymous');

    /** @var \Drupal\authenticate_by_mail\Controller\LoginController */
    $login_controller = \Drupal::classResolver(LoginController::class);
    $login_controller->authenticate($user->id(), $timestamp = \time(), \user_pass_rehash($user, $timestamp));

    $this->assertCount(1, $messages = \Drupal::messenger()->messagesByType(MessengerInterface::TYPE_ERROR));
    $this->assertContainsEquals('The one-time login link you clicked is invalid.', $messages);

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is still anonymous');
  }

  /**
   * Tests login link behavior for a user that was deleted.
   */
  public function testDeletedUser() {
    $user = $this->createUser();
    $user->delete();

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is anonymous');

    /** @var \Drupal\authenticate_by_mail\Controller\LoginController */
    $login_controller = \Drupal::classResolver(LoginController::class);
    $login_controller->authenticate($user->id(), $timestamp = \time(), \user_pass_rehash($user, $timestamp));

    $this->assertCount(1, $messages = \Drupal::messenger()->messagesByType(MessengerInterface::TYPE_ERROR));
    $this->assertContainsEquals('The one-time login link you clicked is invalid.', $messages);

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is still anonymous');
  }

  /**
   * Tests expired login links.
   */
  public function testExpiredLink() {
    $timeout = $this->config('authenticate_by_mail.settings')->get('timeout');
    $this->assertGreaterThan(0, $timeout, 'Has a login link expiry defined');

    $user = $this->createUser();
    $user->setLastLoginTime(\time())->save();

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is anonymous');

    /** @var \Drupal\authenticate_by_mail\Controller\LoginController */
    $login_controller = \Drupal::classResolver(LoginController::class);
    $login_controller->authenticate($user->id(), $timestamp = 0, \user_pass_rehash($user, $timestamp));

    $this->assertCount(1, $messages = \Drupal::messenger()->messagesByType(MessengerInterface::TYPE_ERROR));
    $this->assertContainsEquals('You have tried to use a one-time login link that has expired.', $messages);

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is still anonymous');
  }

  /**
   * Tests that login links don't expire for new users.
   */
  public function testExpiredLinkNewUser() {
    $timeout = $this->config('authenticate_by_mail.settings')->get('timeout');
    $this->assertGreaterThan(0, $timeout, 'Has a login link expiry defined');

    $user = $this->createUser();

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is anonymous');

    /** @var \Drupal\authenticate_by_mail\Controller\LoginController */
    $login_controller = \Drupal::classResolver(LoginController::class);
    $login_controller->authenticate($user->id(), $timestamp = 0, \user_pass_rehash($user, $timestamp));

    $this->assertEmpty(\Drupal::messenger()->messagesByType(MessengerInterface::TYPE_ERROR));
    $this->assertTrue(\Drupal::currentUser()->isAuthenticated(), 'Current user is now authenticated');
  }

  /**
   * Tests login links which were generated prior to the user's last login.
   */
  public function testLinkGeneratedBeforeLastLogin() {
    $user = $this->createUser();
    $user->setLastLoginTime(\time())->save();

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is anonymous');

    /** @var \Drupal\authenticate_by_mail\Controller\LoginController */
    $login_controller = \Drupal::classResolver(LoginController::class);
    $login_controller->authenticate($user->id(), $timestamp = \time() - 1, \user_pass_rehash($user, $timestamp));

    $this->assertCount(1, $messages = \Drupal::messenger()->messagesByType(MessengerInterface::TYPE_ERROR));
    $this->assertContainsEquals('You have tried to use a one-time login link that has either been used or is no longer valid.', $messages);

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is still anonymous');
  }

  /**
   * Tests login links which were generated in the future.
   */
  public function testLinkGeneratedInTheFuture() {
    $user = $this->createUser();

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is anonymous');

    /** @var \Drupal\authenticate_by_mail\Controller\LoginController */
    $login_controller = \Drupal::classResolver(LoginController::class);
    $login_controller->authenticate($user->id(), $timestamp = \time() + 10, \user_pass_rehash($user, $timestamp));

    $this->assertCount(1, $messages = \Drupal::messenger()->messagesByType(MessengerInterface::TYPE_ERROR));
    $this->assertContainsEquals('You have tried to use a one-time login link that has either been used or is no longer valid.', $messages);

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is still anonymous');
  }

  /**
   * Tests login links with invalid hashes.
   */
  public function testLinkWithInvalidHash() {
    $user = $this->createUser();

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is anonymous');

    /** @var \Drupal\authenticate_by_mail\Controller\LoginController */
    $login_controller = \Drupal::classResolver(LoginController::class);
    $login_controller->authenticate($user->id(), \time(), \user_pass_rehash($user, 0));

    $this->assertCount(1, $messages = \Drupal::messenger()->messagesByType(MessengerInterface::TYPE_ERROR));
    $this->assertContainsEquals('You have tried to use a one-time login link that has either been used or is no longer valid.', $messages);

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is still anonymous');
  }

  /**
   * Tests valid login links.
   */
  public function testValidLink() {
    $user = $this->createUser();

    $this->assertTrue(\Drupal::currentUser()->isAnonymous(), 'Current user is anonymous');

    /** @var \Drupal\authenticate_by_mail\Controller\LoginController */
    $login_controller = \Drupal::classResolver(LoginController::class);
    $login_controller->authenticate($user->id(), $timestamp = \time() - 1, \user_pass_rehash($user, $timestamp));

    $this->assertTrue(\Drupal::currentUser()->isAuthenticated(), 'Current user is now authenticated');
  }

}
